package webservice

import java.io.FileWriter
import java.nio.file.Files._
import java.nio.file.Paths._
import java.nio.file.StandardCopyOption._

import db.{DBInterface, HotelRecord}

/**
 *  Presents method to write database response in table in html file
 */
object CreateAnswer {
  /**
   *  Copies saved html head to answer file.
   */
  private def copyAnswerName = {
    implicit def toPath (filename: String) = get(filename)
    copy("src/main/html/answer_head.html", "src/main/html/answer.html", REPLACE_EXISTING)
  }

  /**
   *  Generates table in html answer file by list of hotel records.
   */
  def createAnswerFile(Nans: Int, answer:Seq[HotelRecord]) = {
    copyAnswerName
    try {
      val writer: FileWriter = new FileWriter("src/main/html/answer.html", true)
      val size = DBInterface.Size
      writer.write(s"<body>\nFound $Nans hotels matching request. Total: $size\n")
      writer.write("<table style=\"width:100%\">\n    <tr>\n        <th>Hotel name</th>\n        <th>Rooms</th>\n        <th>Rating</th>\n        <th>Reviews</th>\n        <th>Free parking</th>\n        <th>Free WiFi</th>\n        <th>Safe</th>\n        <th>Restaurant</th>\n        <th>Spa</th>\n        <th>Ski</th>\n        <th>English</th>\n        <th>Russian</th>\n    </tr>")
      for (hotel <- answer){
        writer.write("<tr>")
          for (entry <- hotel.Entries){
            writer.write("<td>"+entry.stringValue+"</td>")
          }
        writer.write("</tr>")
      }
      writer.write("</table></body></html>")
      writer.close
    } catch {
      case _ => None
    }
  }
}
