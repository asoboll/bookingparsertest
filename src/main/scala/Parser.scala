import bookingparser.BookingParser
import db.DBInterface

/**
 *  Application, parsing booking.com hotels in region Tirol, Austria.
 *  Opens database table, starts parsing process. In the end closes table.
 *  Generally, based on HtmlCleaner and Slick database libraries
 */
object Parser extends App {
  DBInterface.New
  BookingParser.ParseHotelsByRegion("at","tirol")
  DBInterface.close
}