import servercore._

/**
 *  Application, running web server
 *  with simple interface to access parsed hotels table.
 *  Based on Spray template.
 */
object Rest extends App with BootedCore with CoreActors with Api with Web