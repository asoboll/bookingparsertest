package db

import slick.driver.MySQLDriver.api._
import slick.lifted.ProvenShape
import myTypedef._

/**
 *  Defines database table structure.
 */
class HotelsTable(tag: Tag)
  extends Table[HotelsTuple](tag, "HotelsTable") {

  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def name: Rep[String] = column[String](HotelRecordDescription._1)
  def Nrooms: Rep[Int] = column[Int](HotelRecordDescription._2)
  def rating: Rep[Double] = column[Double](HotelRecordDescription._3)
  def Nreviews: Rep[Int] = column[Int](HotelRecordDescription._4)
  def parking: Rep[Boolean] = column[Boolean](HotelRecordDescription._5)
  def freewifi: Rep[Boolean] = column[Boolean](HotelRecordDescription._6)
  def safe: Rep[Boolean] = column[Boolean](HotelRecordDescription._7)
  def restaurant: Rep[Boolean] = column[Boolean](HotelRecordDescription._8)
  def spa: Rep[Boolean] = column[Boolean](HotelRecordDescription._9)
  def skiing: Rep[Boolean] = column[Boolean](HotelRecordDescription._10)
  def english: Rep[Boolean] = column[Boolean](HotelRecordDescription._11)
  def russian: Rep[Boolean] = column[Boolean](HotelRecordDescription._12)

  def * : ProvenShape[HotelsTuple] = (id.?, name, Nrooms, rating, Nreviews, parking, freewifi,
    safe,restaurant,spa,skiing,english,russian)
}