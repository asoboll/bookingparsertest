package bookingparser

import java.net.URL
import db._
import org.htmlcleaner.{HtmlCleaner, TagNode}
import scala.collection.mutable.ListBuffer
import scala.util.control.NonFatal

/**
 *  Contains functions to collect hotels information from booking.com
 */
object BookingParser {
  private val cleaner = new HtmlCleaner

  /**
   *  Opens region page on booking with HtmlCleaner,
   *  finds link that shows all hotels in region
   *  and executes parsing hotels with ParseHotelsFromSearchResults
   */
  def ParseHotelsByRegion(countryCode: String, region: String): Unit = {
    try {
      val url = "http://www.booking.com/region/" + countryCode + "/" + region + ".html"
      val rootNode: TagNode = cleaner.clean(new URL(url))
      var elements: Array[TagNode] = rootNode.getElementsByAttValue("class", "allhotelsin", true, false)
      if (!elements.isEmpty) {
        elements = elements(0).getElementsByName("a", true)
        if (!elements.isEmpty) {
          val nexturl = "http://www.booking.com" + elements(0).getAttributeByName("href")
          ParseHotelsFromSearchResults(nexturl)
        }
      }
    } catch {
      case NonFatal(t) => None
    }
  }

  /**
   *  Opens booking page containing search results with HtmlCleaner,
   *  then determines links to all hotels in and executes getHotelRecord for each.
   *  Then starts parsing of the next page
   */
  def ParseHotelsFromSearchResults(url: String): Unit = {
    if (url == "") return
    val rootNode: TagNode = cleaner.clean(new URL(url))
    try {
      val elements: Array[TagNode] = rootNode.getElementsByAttValue("class", "hotel_name_link url", true, false)
      for (elem <- elements) {
        try {
          val nexturl = "http://www.booking.com" + elem.getAttributeByName("href").split("[?]")(0)
          DBInterface.Add(getHotelRecord(nexturl).toTuple)
        } catch {
          case NonFatal(t) => None
        }
      }
    } catch {
      case NonFatal(t) => None
    }
    ParseHotelsFromSearchResults(NextPageOfSearchResults)

    /**
     *  Finds next page url of booking search results if it exists
     */
    def NextPageOfSearchResults: String = {
      var elements: Array[TagNode] = rootNode.getElementsByAttValue("class", "results-paging",true,false)
      if(!elements.isEmpty) {
        elements = elements(0).getElementsByName("a",false)
        if (!elements.isEmpty){
          if(elements.last.getText.toString.trim == "Next page") {
            val nexturl = elements.last.getAttributeByName("href")
            ParseHotelsFromSearchResults(nexturl)
          }
        }
      }
      return ""
    }
  }

  /**
   *  Gathers hotel information by the given url.
   *  Consequentially finds values for different fields of HotelRecord
   */
  def getHotelRecord(url: String): HotelRecord = {
    val Record = new HotelRecord
    val rootNode: TagNode = cleaner.clean(new URL(url))
    val facilities: List[String] = getFacilities(rootNode)
    val rating: (Double, Int) = getHotelRating(rootNode)
    for (i <- 0 to Record.Entries.length - 1) {
      Record.Entries(i) = Record.Entries(i) match {
        case Name(s) => getHotelName(rootNode)
        case Rooms(n) => getNumberOfRooms(rootNode)
        case Rating(rat) => Rating(rating._1)
        case NReviews(nrev) => NReviews(rating._2)
        case Facility(str, bool) => Facility(str, facilities.exists(s => s.contains(str)))
      }
    }
    return Record
  }

  /**
   *  Gathers hotel hotel by the given html root
   */
  private def getHotelName(rootNode:TagNode): HotelEntry = {
    try {
      val elements: Array[TagNode] = rootNode.getElementsByAttValue("id", "hp_hotel_name", true, false)
      if (!elements.isEmpty) {
        return Name(elements(0).getText.toString.trim)
      }
    } catch {
      case NonFatal(t) => None
    }
    return Name("Error parsing hotel name")
  }

  /**
   *  Gathers number of rooms in hotel by the given html root
   */
  private def getNumberOfRooms(rootNode: TagNode): HotelEntry = {
    try {
      var elements: Array[TagNode] = rootNode.getElementsByAttValue("class", "hotel_description_wrapper_exp", true, false)
      if (!elements.isEmpty) {
        elements = elements(0).getElementsByAttValue("class", "summary", true, false)
        if (!elements.isEmpty) {
          val str = elements(0).getText.toString
          return Rooms(str.split(": ")(1).split("[^\\d]")(0).toInt)
        }
      }
    } catch{
      case e : java.lang.ArrayIndexOutOfBoundsException => return Rooms(1)
      case e : java.lang.NumberFormatException => return Rooms(1)
      case NonFatal(t) => None
    }
    return Rooms(-1);
  }

  /**
   *  Gathers hotel rating by the given html root
   */
  private def getHotelRating(rootNode: TagNode): (Double, Int) = {
    try {
      val elements: Array[TagNode] = rootNode.getElementsByAttValue("class", "hotel_large_photp_score featured_review_score", true, false)
      if (!elements.isEmpty) {
        val elems1 = elements(0).getElementsByAttValue("class", "rating notranslate", true, false)
        val elems2 = elements(0).getElementsByAttValue("class", "count", true, false)
        if (elems1.nonEmpty && elems2.nonEmpty) {
          val str = elems1(0).getText.toString.trim
          val rat = str.split("/")(0).toDouble
          val rev = elems2(0).getText.toString.replaceAll("[^\\d]", "").toInt
          return (rat, rev)
        }
      }
    } catch {
      case NonFatal(t) => None
    }
    return (-1.0,0)
  }

  /**
   *  Gathers list of hotel's facilities by the given html root
   */
  private def getFacilities(rootNode: TagNode): List[String] = {
    var facilities = new ListBuffer[String]
    try {
      val elements: Array[TagNode] = rootNode.getElementsByAttValue("class", "facilitiesChecklistSection", true, false)
      for (elem <- elements) {
        val headline = elem.getChildTags()(0).getText.toString.trim
        if((headline == "Parking") || (headline == "Internet")) {
          if(elem.getElementsByAttValue("class","hp-free-facility-row",true,false).nonEmpty)
            facilities += headline
        }
        else {
          val titles = elem.getElementsByName("li", true)
          for (title <- titles) {
            facilities += title.getText.toString.trim
          }
        }
      }
    } catch {
      case NonFatal(t) => None
    }
    return facilities.toList
  }
}
