package db

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import slick.driver.MySQLDriver.api._
import myTypedef._

/**
 *  Presents database interface to write and read hotel records.
 *  Based on slick database library
 */
object DBInterface{
  val db = Database.forConfig("mysql1")
  val hotelsTable: TableQuery[HotelsTable] = TableQuery[HotelsTable]

  /**
   *  Creates or cleans table in database.
   */
  def New: Unit = {
    try {
      Await.result(db.run(hotelsTable.schema.create), Duration.Inf)
    } catch{
      case e : com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException => {
        Drop
        Await.result(db.run(hotelsTable.schema.create), Duration.Inf)
      }
    }
  }

  /**
   *  By the given filter criteria builds database request.
   *  Limits response to 100 entries.
   *  Returns number of hotels found and list of hotel records
   */
  def FormRequest(nrooms1:Int, nrooms2:Int, rating1:Double, rating2:Double, nreviews1:Int, nreviews2:Int, parking:String,
                  internet:String, safe:String, restaurant:String, spa:String, ski:String, english:String, russian:String
                   ): (Int,Seq[HotelRecord]) = {

    var req = hotelsTable.filter(_.Nrooms >= nrooms1).filter(_.Nrooms <= nrooms2).filter(_.rating >= rating1).filter(_.rating <= rating2)
    .filter(_.Nreviews >= nreviews1).filter(_.Nreviews <= nreviews2)

    def booleanfilter(f:HotelsTable => Rep[Boolean])(s:String) = {
      req = s match {
        case "Yes" => req.filter(x=>{f(x)})
        case "No" => req.filter(x=>{!f(x)})
        case "Select" => req
      }
    }
    booleanfilter(_.parking)(parking)
    booleanfilter(_.freewifi)(internet)
    booleanfilter(_.safe)(safe)
    booleanfilter(_.restaurant)(restaurant)
    booleanfilter(_.spa)(spa)
    booleanfilter(_.skiing)(ski)
    booleanfilter(_.english)(english)
    booleanfilter(_.russian)(russian)
    val Nans: Int = Await.result(db.run(req.length.result),Duration.Inf)
    val answer = Await.result(db.run(req.take(100).result),Duration.Inf)
    return (Nans, answer.map(x => {new HotelRecord(x)}))
  }

  /**
   *  Returns total bumber of hotels in table
   */
  def Size: Int = {
    return Await.result(db.run(hotelsTable.size.result),Duration.Inf)
  }

  /**
   *  Adds new hotel record to table
   */
  def Add(hotel:HotelsTuple): Unit = {
    Await.result(db.run(hotelsTable += hotel), Duration.Inf)
  }

  /**
   *  Drops the table
   */
  def Drop: Unit = {
    Await.result(db.run(hotelsTable.schema.drop), Duration.Inf)
  }

  def close: Unit = {
    db.close
  }
}
