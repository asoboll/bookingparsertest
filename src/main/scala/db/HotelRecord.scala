package db

/**
 *  Defines type HotelsTuple containing hotel information.
 *  Defines description of hotel record fields for database table.
 *  Defines auxiliary functions to extract data from HotelEntries
 */
object myTypedef {
  type HotelsTuple = (Option[Int], String, Int, Double, Int, Boolean, Boolean,
    Boolean,Boolean,Boolean,Boolean,Boolean,Boolean)
  val HotelRecordDescription = ("Hotel_Name", "Number_Of_Rooms", "Rating", "Number_Of_Reviews", "Free_Parking", "Free_WiFi",
   "Safe", "Restaurant", "Spa", "Ski", "English", "Russian")
  def mytoString(x:HotelEntry):String = x match{case Name(s) => s}
  def mytoInt(x:HotelEntry):Int = x match{case Rooms(i) => i; case NReviews(i) => i}
  def mytoDouble(x:HotelEntry):Double = x match{case Rating(d)=>d}
  def mytoBoolean(x:HotelEntry):Boolean = x match{case Facility(s,b)=>b}
}
import myTypedef._

/**
 *  Contains hotel description in form of HotelEntry array.
 *  Presents methods to convert record from and to Hotelstuple
 */
class HotelRecord(htuple: HotelsTuple) {
  var Entries:Array[HotelEntry] = Array(
    Name(htuple._2),
    Rooms(htuple._3),
    Rating(htuple._4),
    NReviews(htuple._5),
    Facility("Parking", htuple._6),
    Facility("Internet", htuple._7),
    Facility("Safe", htuple._8),
    Facility("Restaurant", htuple._9),
    Facility("Spa", htuple._10),
    Facility("Ski", htuple._11),
    Facility("English", htuple._12),
    Facility("Russian", htuple._13)
  )

  def this() {
    this((None,"",0,-1,0,false,false,false,false,false,false,false,false))
  }

  def toTuple: HotelsTuple = {
    return (None, mytoString(Entries(0)),mytoInt(Entries(1)),mytoDouble(Entries(2)),mytoInt(Entries(3)),
      mytoBoolean(Entries(4)),mytoBoolean(Entries(5)),mytoBoolean(Entries(6)),mytoBoolean(Entries(7)),
      mytoBoolean(Entries(8)),mytoBoolean(Entries(9)),mytoBoolean(Entries(10)),mytoBoolean(Entries(11)))
  }
}

/**
 *  Case classes for different hotel information with method returning value in string
 */
abstract class HotelEntry {def stringValue: String}
case class Name(hotelName: String) extends HotelEntry {def stringValue = hotelName}
case class Rooms(numberOfRooms: Int) extends HotelEntry {def stringValue = numberOfRooms.toString}
case class Rating(rating: Double) extends HotelEntry {def stringValue = rating.toString}
case class NReviews(nreviews: Int) extends HotelEntry {def stringValue = nreviews.toString}
case class Facility(facilityName: String, presence: Boolean) extends HotelEntry {def stringValue = presence.toString}
