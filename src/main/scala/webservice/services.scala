package webservice

import db.DBInterface
import spray.http.StatusCodes._
import spray.http._
import spray.routing._
import spray.util.{SprayActorLogging, LoggingContext}
import util.control.NonFatal
import akka.actor.Actor

/**
 * Holds potential error response with the HTTP status and optional body
 *
 * @param responseStatus the status code
 * @param response the optional body
 */
case class ErrorResponseException(responseStatus: StatusCode, response: Option[HttpEntity]) extends Exception

/**
 * Allows you to construct Spray ``HttpService`` from a concatenation of routes; and wires in the error handler.
 * It also logs all internal server errors using ``SprayActorLogging``.
 */
class RoutedHttpService extends Actor with HttpService with SprayActorLogging {

  implicit def actorRefFactory = context

  implicit val handler = ExceptionHandler {
    case NonFatal(ErrorResponseException(statusCode, entity)) => ctx =>
      ctx.complete(statusCode, entity)

    case NonFatal(e) => ctx => {
      log.error(e, InternalServerError.defaultMessage)
      ctx.complete(InternalServerError)
    }
  }

  /**
   *  Defines accessible paths on the web server,
   *  including request form and answer pages.
   *  Post method collects request and
   *  executes generation of answer page.
   */
  private val route = path ("hello") {
    complete("hello")
  }~ path("form"){
    getFromFile("src/main/html/form.html")
  }~ path ("answer") {
    getFromFile("src/main/html/answer.html")
  }~ path ("results") {
    post {
      dynamic {
        formFields('nrooms1.as[Int], 'nrooms2.as[Int], 'rating1.as[Double], 'rating2.as[Double], 'nreviews1.as[Int],
          'nreviews2.as[Int], 'parking, 'internet, 'safe, 'restaurant, 'spa, 'ski, 'english, 'russian) {
          (nrooms1, nrooms2, rating1, rating2, nreviews1, nreviews2, parking, internet, safe, restaurant, spa, ski, english, russian) => {
            val hotelsvector = DBInterface.FormRequest(nrooms1, nrooms2, rating1, rating2, nreviews1, nreviews2, parking, internet, safe, restaurant, spa, ski, english, russian)
            CreateAnswer.createAnswerFile(hotelsvector._1, hotelsvector._2)
            redirect("answer", StatusCodes.SeeOther)
          }
        }
      }
    }
  }~ path("") {
    redirect("form",StatusCodes.PermanentRedirect)
  }

  def receive: Receive = {
    runRoute(route)(handler, RejectionHandler.Default, context, RoutingSettings.default, LoggingContext.fromActorRefFactory)
  }
}